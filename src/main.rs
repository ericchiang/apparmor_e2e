use std::env;
use std::ffi;
use std::fs;
use std::io;
use std::path;
use std::process;
use std::ptr;
use std::result;

#[macro_use]
extern crate failure;
extern crate libc;

mod e2e;
mod flag;
mod qemu;

static BOOT_IMAGE: &'static str = "BOOT_IMAGE=";

type Result<T> = result::Result<T, failure::Error>;

fn main() {
    match run() {
        Ok(()) => {}
        Err(err) => {
            eprintln!("error: {}", err);
            process::exit(1);
        }
    };
}

fn run() -> Result<()> {
    if qemu::in_qemu() {
        if path::Path::new("/proc/self").exists() {
            // Running as sub-test not the test suite itself.
            return run_test();
        }
        let r = run_tests();
        if r.is_err() {
            eprintln!("{}", r.err().unwrap());
            println!("{}", qemu::TESTS_FAILED);
        }
        unsafe { libc::reboot(libc::LINUX_REBOOT_CMD_RESTART) };
        return Ok(());
    }
    let args: Vec<String> = env::args().collect();
    return reexec(args[1..].to_vec());
}

fn run_tests() -> Result<()> {
    mount("sysfs", "/sys", "sysfs")?;
    mount("proc", "/proc", "proc")?;
    mount("udev", "/dev", "devtmpfs")?;
    mount("securityfs", "/sys/kernel/security", "securityfs")?;

    let mut failed = false;
    for test in e2e::tests() {
        eprintln!("[+] Running test: {}", test.name());

        test.setup()
            .map_err(|e| format_err!("setting up test {} {}", test.name(), e))?;
        let rc = process::Command::new(test.name())
            .spawn()
            .map_err(|e| format_err!("starting test {} {}", test.name(), e))?
            .wait()
            .map_err(|e| format_err!("waiting for test {} to exit {}", test.name(), e))?;
        test.teardown()
            .map_err(|e| format_err!("tear down test {} {}", test.name(), e))?;

        if rc.success() {
            eprintln!("[+] Passed");
        } else {
            failed = true;
            eprintln!("[+] Failed");
        }
    }

    if failed {
        return Err(failure::err_msg("e2e suite failed"));
    }
    return Ok(());
}

fn run_test() -> Result<()> {
    let curr_exe = env::current_exe()
        .map_err(|e| format_err!("getting current executable {}", e))?
        .to_string_lossy()
        .to_string();

    let tests = e2e::tests();
    for test in tests {
        if test.name() == curr_exe {
            test.run();
            return Ok(());
        }
    }
    return Err(format_err!("no test registered for path {}", curr_exe));
}

fn cstring(s: &str) -> Result<ffi::CString> {
    return ffi::CString::new(s).map_err(|e| format_err!("allocating cstring {}", e));
}

fn mount(dev: &str, path: &str, fstype: &str) -> Result<()> {
    let d = cstring(dev)?;
    let p = cstring(path)?;
    let f = cstring(fstype)?;
    let rc = unsafe { libc::mount(d.as_ptr(), p.as_ptr(), f.as_ptr(), 0, ptr::null()) };
    if rc == -1 {
        return Err(format_err!(
            "mount {} {}",
            path.to_string(),
            io::Error::last_os_error()
        ));
    }
    return Ok(());
}

fn usage() {
    println!(
        "Usage: apparmor_e2e [flags]

A QEMU based tool for testing AppArmor parser and kernel interaction.

Flags:
    
    --parser    Path to the apparmor_parser. Defaults to /sbin/apparmor_parser.
    --kernel    Path to the kernel image. Defaults to the system's BOOT_IMAGE= value.
    -h, --help  Display this message
"
    );
}

fn reexec(args: Vec<String>) -> Result<()> {
    let r = flag::FlagSet::new()
        .flag(flag::Flag::new().short("h").long("help"))
        .flag(flag::Flag::new().long("parser").takes_value())
        .flag(flag::Flag::new().long("kernel").takes_value())
        .parse(args)
        .map_err(|e| format_err!("parsing flags {}", e))?;

    if r.present("help") {
        usage();
        return Ok(());
    }

    if r.args().len() > 0 {
        return Err(format_err!("too many arguments provided"));
    }

    let kernel_path = match r.value("kernel") {
        Some(val) => val.to_string(),
        None => {
            // Attempt to determine the path to kernel based on the BOOT_IMAGE kernel
            // command line variable.
            let mut kernel_cmdline = fs::read_to_string("/proc/cmdline").map_err(|e| {
                format_err!("determining path for current system's kernel image {}", e)
            })?;
            let kernel_path = boot_image(&mut kernel_cmdline)?;
            fs::metadata(kernel_path.clone())
                .map_err(|e| format_err!("BOOT_IMAGE path {} error {}", kernel_path, e))?;
            kernel_path
        }
    };

    let apparmor_parser = match r.value("parser") {
        Some(val) => val.to_string(),
        None => "/sbin/apparmor_parser".to_string(),
    };

    let curr_exe = env::current_exe()
        .map_err(|e| format_err!("determining currrent executable {}", e))?
        .to_string_lossy()
        .to_string();

    let mut r = qemu::Runner::new();
    r.add(curr_exe.clone(), "/init".to_string());
    r.add(apparmor_parser, "/sbin/apparmor_parser".to_string());

    for test in e2e::tests() {
        r.add(curr_exe.clone(), test.name().to_string());
    }

    return r
        .run(kernel_path)
        .map_err(|e| format_err!("running QEMU {}", e));
}

/// Guess the path of the system's kernel based on the BOOT_IMAGE kernel command
/// line variable.
fn boot_image(cmdline: &mut String) -> Result<String> {
    let rest = match cmdline.match_indices(BOOT_IMAGE).next() {
        None => {
            return Err(format_err!(
                "no BOOT_IMAGE command line entry found, provide a kernel path manually",
            ));
        }
        Some((i, _)) => cmdline.split_off(i + BOOT_IMAGE.len()),
    };

    return match rest.split_whitespace().next() {
        None => {
            return Err(format_err!(
                "no BOOT_IMAGE command line entry found, provide a kernel path manually",
            ));
        }
        Some(s) => Ok("/boot/".to_string() + &s),
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_boot_image() {
        let mut s = "BOOT_IMAGE=foo".to_string();
        let img = boot_image(&mut s).unwrap();
        assert_eq!("/boot/foo".to_string(), img);
    }

    #[test]
    fn test_boot_image_with_trailer() {
        let mut s = "BOOT_IMAGE=foo another=value".to_string();
        let img = boot_image(&mut s).unwrap();
        assert_eq!("/boot/foo".to_string(), img);
    }

    #[test]
    fn test_boot_image_with_other_values() {
        let mut s = "foo=bar BOOT_IMAGE=foo another=value".to_string();
        let img = boot_image(&mut s).unwrap();
        assert_eq!("/boot/foo".to_string(), img);
    }
}
