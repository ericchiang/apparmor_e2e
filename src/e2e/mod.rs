use std::env;
use std::error;
use std::ffi;
use std::fs;
use std::io;
use std::os::unix::fs as unixfs;
use std::path;
use std::process;
use std::result;

static APPARMOR_PARSER: &str = "/sbin/apparmor_parser";

pub trait Test {
    /// Name of the tests. This should be a path to be re-exec'ed by the test
    /// framework.
    fn name(&self) -> &str;
    /// Work to do before exec'ing the test.
    fn setup(&self) -> io::Result<()>;
    /// Work to do after exec'ing the test.
    fn teardown(&self) -> io::Result<()>;

    /// Re-exec'd test logic. This method should panic if the test fails.
    fn run(&self);
}

// Assert the the result succeeded, panicing on any returned error. This is
// different than 'assert!(r.is_ok, "msg")' which doesn't print the result.
fn assert_ok<R, E: error::Error>(r: result::Result<R, E>, msg: &str) {
    if r.is_err() {
        panic!("{} {}", msg, r.err().unwrap());
    }
}

// Assert the the result failed, panicing otherwise.
fn assert_err<R, E>(r: result::Result<R, E>, msg: &str) {
    if r.is_ok() {
        panic!("{} succeeded", msg);
    }
}

/// Returns all e2e tests registered by this module.
pub fn tests<'a>() -> Vec<&'a Test> {
    // Register new tests by adding them to this vector.
    return vec![&TestFiles {}, &TestRegexPriority {}, &TestChangeDir {}];
}

fn init_profiles() -> io::Result<()> {
    fs::create_dir_all("/etc/apparmor.d/abstractions")?;
    fs::create_dir_all("/etc/apparmor.d/tunables")?;
    fs::create_dir_all("/etc/apparmor")?;
    fs::write("/etc/apparmor/parser.conf", "")?;
    fs::write(
        "/etc/apparmor.d/abstractions/test",
        "
  # ld.so.cache and ld are used to load shared libraries; they are best
  # available everywhere
  /etc/ld.so.cache               mr,
  /etc/ld.so.conf                r,
  /etc/ld.so.conf.d/{,*.conf}    r,
  /etc/ld.so.preload             r,
  /{usr/,}lib{,32,64}/ld{,32,64}-*.so   mr,
  /{usr/,}lib/tls/i686/{cmov,nosegneg}/ld-*.so     mr,
  /{usr/,}lib/i386-linux-gnu/tls/i686/{cmov,nosegneg}/ld-*.so     mr,
  /opt/*-linux-uclibc/lib/ld-uClibc*so* mr,

  # we might as well allow everything to use common libraries
  /{usr/,}lib{,32,64}/**                r,
  /{usr/,}lib{,32,64}/lib*.so*          mr,
  /{usr/,}lib{,32,64}/**/lib*.so*       mr,
  /{usr/,}lib/tls/i686/{cmov,nosegneg}/lib*.so*    mr,
  /{usr/,}lib/i386-linux-gnu/tls/i686/{cmov,nosegneg}/lib*.so*    mr,

  /proc/** r,
  /dev/null r,
",
    )?;
    return Ok(());
}

fn profile_path<P: AsRef<path::Path>>(p: P) -> String {
    let file = p.as_ref().to_string_lossy().replace("/", ".");
    return path::Path::new("/etc/apparmor.d")
        .join(file)
        .to_string_lossy()
        .to_string();
}

fn load_profile(name: &str, profile: &str) -> io::Result<()> {
    init_profiles()?;
    let p = profile_path(name);
    fs::write(p.clone(), profile)?;
    process::Command::new(APPARMOR_PARSER)
        .arg("--add")
        .arg("--skip-cache")
        .arg("--base")
        .arg("/etc/apparmor.d")
        .arg(p.clone())
        .spawn()?
        .wait()?;

    return Ok(());
}

fn remove_profile(name: &str) -> io::Result<()> {
    process::Command::new(APPARMOR_PARSER)
        .arg("--remove")
        .arg("--skip-cache")
        .arg(profile_path(name))
        .spawn()?
        .wait()?;
    return Ok(());
}

fn unlink(path: &str) -> io::Result<()> {
    let p = ffi::CString::new(path).unwrap(); // panic on path with NULL character
    let rc = unsafe { libc::unlink(p.as_ptr()) };
    if rc == 0 {
        return Ok(());
    }
    return Err(io::Error::last_os_error());
}

/// Exercises basic file access.
struct TestFiles {}

impl Test for TestFiles {
    fn name(&self) -> &str {
        return &"/usr/sbin/apparmor_e2e_files";
    }

    fn setup(&self) -> io::Result<()> {
        load_profile(
            "files",
            "
profile files /usr/sbin/apparmor_e2e_files {
    #include<abstractions/test>

    / r,
    /etc/ r,
    /etc/test rw,
    /etc/link rw,
    /etc/link_target lwr,
}
",
        )?;
        return Ok(());
    }

    fn teardown(&self) -> io::Result<()> {
        remove_profile("files")?;
        return Ok(());
    }

    fn run(&self) {
        assert_ok(fs::read_dir("/"), "reading /");
        assert_ok(fs::read_dir("/etc"), "reading /etc");
        assert_ok(fs::write("/etc/test", ""), "writing /etc/test");
        assert_ok(fs::remove_file("/etc/test"), "rm /etc/test");

        assert_err(fs::write("/etc/bad", ""), "writing /etc/bad");
        assert_err(fs::read_dir("/etc/apparmor.d"), "read /etc/apparmor.d");
        assert_err(fs::remove_dir_all("/etc"), "rm -r /etc");

        assert_ok(fs::write("/etc/link_target", ""), "touch /etc/link_target");
        assert_ok(
            unixfs::symlink("/etc/link_target", "/etc/link"),
            "ln -s /etc/link_target /etc/link",
        );
        assert_ok(unlink("/etc/link"), "unlink /etc/link");

        // 'r' implies 'l'
        assert_ok(
            unixfs::symlink("/etc/test", "/etc/link"),
            "ln -s /etc/test /etc/link",
        );
        assert_ok(unlink("/etc/link"), "unlink /etc/link");

        // creating the link still requires 'w'
        assert_err(
            unixfs::symlink("/etc/link_target", "/etc/link_bad"),
            "ln -s /etc/test /etc/link_bad",
        );

        // cleanup files
        assert_ok(fs::remove_file("/etc/link_target"), "rm /etc/link_target");
    }
}

/// Test that full paths beat regexes
struct TestRegexPriority {}

impl Test for TestRegexPriority {
    fn name(&self) -> &str {
        return "/usr/sbin/apparmor_e2e_regex_priority";
    }

    fn setup(&self) -> io::Result<()> {
        load_profile(
            "profile_1",
            "
profile profile_1 /usr/sbin/apparmor_e2e_regex_* {
         #include<abstractions/test>
}
",
        )?;
        load_profile(
            "profile_2",
            "
profile profile_2 /usr/sbin/apparmor_e2e_regex_priority {
    #include<abstractions/test>
}
",
        )?;
        return Ok(());
    }

    fn teardown(&self) -> io::Result<()> {
        remove_profile("profile_1")?;
        remove_profile("profile_2")?;
        return Ok(());
    }

    fn run(&self) {
        assert_eq!(
            fs::read_to_string("/proc/self/attr/current")
                .unwrap()
                .trim(),
            "profile_2 (enforce)"
        );
    }
}

/// Ensure that changing directory isn't restricted.
struct TestChangeDir {}

impl Test for TestChangeDir {
    fn name(&self) -> &str {
        return "/usr/sbin/apparmor_e2e_cd";
    }

    fn setup(&self) -> io::Result<()> {
        return load_profile(
            "test_cd",
            "
profile test_cd /usr/sbin/apparmor_cd {
    #include<abstractions/test>
}
",
        );
    }

    fn teardown(&self) -> io::Result<()> {
        return remove_profile("test_cd");
    }

    fn run(&self) {
        assert_ok(env::set_current_dir("/sys"), "cd /sys");
    }
}
