# Experimental AppArmor E2E Test Suite

A QEMU-based, end-to-end test harness for the AppArmor parser and Kernel
subsystem. The tool dynamically creates an initrd, running tests as the init
process within the guest. The goals of the tool are:

* test arbirary parser / kernel combinations
* run extremely fast (<1 min)
* minimal host dependencies

This tool was inspired by bradfitz@'s [embiggen-disk][embiggen-disk] integration
tests.

[embiggen-disk]: https://github.com/google/embiggen-disk

## Building

### Installation

`apparmor_e2e` requires QEMU and Rust's `cargo`. Install QEMU through your host
package manager, and [Rust using rustup][rustup]:

```bash
sudo apt-get install qemu-system-x86
curl https://sh.rustup.rs -sSf | sh
```

Clone the repo and run `cargo build` to build the tool:

```bash
git clone https://gitlab.com/apparmor/apparmor_e2e
cd apparmor_e2e
cargo build
```

[rustup]: https://www.rust-lang.org/tools/install

### Running

`apparmor_e2e` tests are self-contained and can be run using `cargo`: 

```bash
cargo run
```

or by running the binary directly:

```bash
cargo build --release
./target/release/apparmor_e2e
```

By default, the tool will use the hosts default `apparmor_parser` and kernel
image. To specify a different parser or image, use the `--parser` and `--kernel`
flags:

```bash
$ cargo run -- \
    --parser /usr/sbin/apparmor_parser \
    --kernel "/boot/vmlinuz-$( uname -r )"
```

## Contributing

See [_"Adding a New Test"_](./docs/adding-a-test.md) for instructions on
contributing an E2E test.

Code is formatted using `rustfmt` for consistency. Use the following commands to
install and run the formatter:

```bash
rustup component add rustfmt
cargo fmt
```

## Why Rust?

AppArmor uses a combination of C, C++, Python, Perl, and Bash for its build and
test suites. Because interpreters increase and convolute a script's dependencies,
to keep the initrd minimal, a compiled language was preferred.

Rust was choosen over C/C++ for development velocity (std library capabilities
and unit testing story). This tool may be re-written in C++ in the future for
alignment with AppArmor's current build tooling.
